const config = require("./config.js");

const baseUrl = config.baseUrl;
const app = getApp({
	allowDefault: true
});

const http = ({
	url = '',
	param = {},
	...other
} = {}) => {
	return new Promise((resolve, reject) => {
		// wx.showLoading({

		// });
		wx.request({
			url: getUrl(url),
			data: param,
			header: {
				'content-type': 'application/json',
				// 默认值 ,另一种是
				// "content-type": "application/x-www-form-urlencoded"
				//获取缓存中的手机号登录的token   
				'token': wx.getStorageSync('token')
			},
			...other,
			complete: res => {
				console.log("--------------------------------------返回数据--------------------------")
				console.log(JSON.stringify(res))
				wx.hideLoading();
				if (res.statusCode >= 200 && res.statusCode < 300) {
					if (res.data.code == 0 || 200 == res.data.code) {
						resolve(res.data);
					} else {
						wx.showModal({
							title: "提示",
							content: res.data.msg,
							confirmText: "确定",
							showCancel: false,
							confirmColor: "#298ff1",
						})
						//resolve(res.data);
					}
				} else if (res.statusCode == 408) {
					wx.showModal({
						title: '提示',
						content: '请求超时',
						confirmText: "确定",
						showCancel: false,
						confirmColor: "#298ff1",
					});
				} else {
					wx.showModal({
						title: '提示',
						content: '服务器内部错误',
						confirmText: "确定",
						confirmColor: "#298ff1",

						success(res) {
							if (res.confirm) {
								wx.removeStorageSync("wxtoken");
								wx.removeStorageSync("earthwork_selected");
								wx.reLaunch({
									url: '/pages/login/login'
								});
							} else if (res.cancel) {
								wx.removeStorageSync("wxtoken");
								wx.removeStorageSync("earthwork_selected");
								wx.reLaunch({
									url: '/pages/login/login'
								});
							}
						}

					});
				}
			}
		});
	});
};

const httpFormdata = ({
	url = '',
	param = {},
	...other
} = {}) => {
	return new Promise((resolve, reject) => {
		// wx.showLoading({

		// });
		wx.request({
			url: getUrl(url),
			data: param,
			header: {
				// 'content-type': 'application/json',
				// 默认值 ,另一种是
				"content-type": "application/x-www-form-urlencoded",
				//获取缓存中的手机号登录的token   
				'token': wx.getStorageSync('token')
			},
			...other,
			complete: res => {
				console.log("--------------------------------------返回数据--------------------------")
				console.log(JSON.stringify(res))
				wx.hideLoading();
				if (res.statusCode >= 200 && res.statusCode < 300) {
					if (res.data.code == 0 || 200 == res.data.code) {
						resolve(res.data);
					} else {
						wx.showModal({
							title: "提示",
							content: res.data.msg,
							confirmText: "确定",
							showCancel: false,
							confirmColor: "#298ff1",
						})
						//resolve(res.data);
					}
				} else if (res.statusCode == 408) {
					wx.showModal({
						title: '提示',
						content: '请求超时',
						confirmText: "确定",
						showCancel: false,
						confirmColor: "#298ff1",
					});
				} else {
					wx.showModal({
						title: '提示',
						content: '服务器内部错误',
						confirmText: "确定",
						confirmColor: "#298ff1",

						success(res) {
							if (res.confirm) {
								wx.removeStorageSync("wxtoken");
								wx.removeStorageSync("earthwork_selected");
								wx.reLaunch({
									url: '/pages/login/login'
								});
							} else if (res.cancel) {
								wx.removeStorageSync("wxtoken");
								wx.removeStorageSync("earthwork_selected");
								wx.reLaunch({
									url: '/pages/login/login'
								});
							}
						}

					});
				}
			}
		});
	});
};

const getUrl = url => {
	if (!config.isTest && url.indexOf('://') == -1) {
		url = baseUrl + url;
	}

	return url;
}; // get方法


const get = (url, param = {}) => {
	return http({
		url,
		param
	});
};

const postFormdata = (url, param = {}) => {
	return httpFormdata({
		url,
		param,
		method: 'post'
	});
};

const post = (url, param = {}) => {
	return http({
		url,
		param,
		method: 'post'
	});
};

const put = (url, param = {}) => {
	return http({
		url,
		param,
		method: 'put'
	});
};

const deletes = (url, param = {}) => {
	return http({
		url,
		param,
		method: 'delete'
	});
};

const apiRequset = (APIINFO, params = {}) => {
	return new Promise((resolve, reject) => {
		try {
			let data;

			if (APIINFO.method == "post" || APIINFO.method == "POST") {
				data = post(APIINFO.url, params);
			} else if (APIINFO.method == "get" || APIINFO.method == "GET") {
				data = get(APIINFO.url, params);
			} else if (APIINFO.method == "put" || APIINFO.method == "PUT") {
				data = put(APIINFO.url, params);
			} else if (APIINFO.method == "delete" || APIINFO.method == "DELETE") {
				data = deletes(APIINFO.url, params);
			}

			resolve(data);
		} catch (e) {
			reject(e);
		}
	});
};

module.exports = {
	baseUrl,
	get,
	post,
	put,
	deletes,
	apiRequset,
	postFormdata
};
