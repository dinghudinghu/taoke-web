//小程序所有的接口地址在此处配置
const getCommodities= {
	url: '/api/commodity/getCommodities',
	desc: '首页获取商品列表',
	method: 'get',
	//接口作者
	author: "dinghu",
	//对接人
	docking: "dinghu"

};

const getPickCommodities= {
	url: '/api/commodityPick/getPickCommodities',
	desc: '首页获取商品列表',
	method: 'get',
	//接口作者
	author: "dinghu",
	//对接人
	docking: "dinghu"

};

const chainChangeTb= {
	url: '/api/chain/change/tb',
	desc: '获取淘宝返利链接',
	method: 'get',
	//接口作者
	author: "dinghu",
	//对接人
	docking: "dinghu"

};

const searchCouponTb= {
	url: '/api/coupon/souquan/taobao',
	desc: '搜淘宝优惠券',
	method: 'post',
	//接口作者
	author: "dinghu",
	//对接人
	docking: "dinghu"

};

const searchCouponJd= {
	url: '/api/coupon/souquan/jingdong',
	desc: '搜淘宝优惠券',
	method: 'post',
	//接口作者
	author: "dinghu",
	//对接人
	docking: "dinghu"

};


module.exports = {
	getCommodities,
	getPickCommodities,
	chainChangeTb,
	searchCouponTb,
	searchCouponJd
};
