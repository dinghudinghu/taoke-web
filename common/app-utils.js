function getTaobaoItemIdFromUrl(url) {
	if (url.startsWith("https://item.taobao.com/item.htm?id=")) {
		console.log("startsWith:1");
		return url.replace("https://item.taobao.com/item.htm?id=", "");
	} else if (url.startsWith("https://detail.tmall.com/item.htm?id=")) {
		console.log("startsWith:2");
		return url.replace("https://detail.tmall.com/item.htm?id=", "");
	}
	if (url.startsWith("http://item.taobao.com/item.htm?id=")) {
		console.log("startsWith:3");
		return url.replace("http://item.taobao.com/item.htm?id=", "");
	} else if (url.startsWith("http://detail.tmall.com/item.htm?id=")) {
		console.log("startsWith:4");
		return url.replace("http://detail.tmall.com/item.htm?id=", "");
	}

}

function isTaobaoUrl(url) {
	return url != null && url != '' && url.indexOf("taobao.com") != -1 || url.indexOf("tmall.com") != -1
}

function isJingdongUrl(url) {
	return url != null && url != '' && url.indexOf("jd.com") != -1 || url.indexOf("jingdong.com") != -1
}

module.exports = {
	getTaobaoItemIdFromUrl: getTaobaoItemIdFromUrl,
	isTaobaoUrl: isTaobaoUrl,
	isJingdongUrl: isJingdongUrl,
}
