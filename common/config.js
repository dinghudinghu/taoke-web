const wxAppID = "wx8c3f73866ada6cba"
const wxAppSecret = "5a04fc50e6ad4f3fc5c7512d470ec45b"
const testBaseUrl = "https://yangmao.huiyaotech.cn/betterlife"

const baseUrl = "https://yangmao.huiyaotech.cn/betterlife";

const isTest = false;

module.exports = {
	isTest: isTest,
	baseUrl: isTest ? testBaseUrl : baseUrl
};
