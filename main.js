import Vue from 'vue'
import App from './App'
import elementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";

Vue.use(elementUI);

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	...App
})
app.$mount()
